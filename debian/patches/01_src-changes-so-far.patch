From: Debian Security Tools <team+pkg-security@tracker.debian.org>
Date: Fri, 14 Sep 2018 16:52:16 +0800
Subject: This patch contains all source code changes done to 0.71 upstream
 version.

---
 src/knocker_args.c | 15 +++++++++++-
 src/knocker_args.h |  7 ++++++
 src/knocker_core.c | 70 +++++++++++++++++++++++++++++++++++++++++-------------
 src/knocker_core.h | 11 +++++----
 src/knocker_main.c |  2 +-
 src/knocker_user.c |  2 +-
 6 files changed, 83 insertions(+), 24 deletions(-)

diff --git a/src/knocker_args.c b/src/knocker_args.c
index 5003831..5ca4c2c 100644
--- a/src/knocker_args.c
+++ b/src/knocker_args.c
@@ -80,9 +80,11 @@ static void print_help_info (void)
   fprintf (stdout, "      %s              performs again the last port scan\n", LAST_SCAN_LONG_OPT);
   fprintf (stdout, "\n");
   fprintf (stdout, "Extra options:\n");
+  fprintf (stdout, "      %s,  %s              only IPv4 host addressing\n", HOST_IPV4_SHORT_OPT, HOST_IPV4_LONG_OPT);
+  fprintf (stdout, "      %s,  %s              only IPv6 host addressing\n", HOST_IPV6_SHORT_OPT, HOST_IPV6_LONG_OPT);
   fprintf (stdout, "      %s,  %s             quiet mode (no console output, logs to file)\n", QUIET_MODE_SHORT_OPT, QUIET_MODE_LONG_OPT);
   fprintf (stdout, "      %s, %s <logfile> log scan results to the specified file\n", ENABLE_LOGFILE_SHORT_OPT, ENABLE_LOGFILE_LONG_OPT);
-  fprintf (stdout, "      %s, %s          disable fency output\n", NO_FENCY_SHORT_OPT, NO_FENCY_LONG_OPT);
+  fprintf (stdout, "      %s, %s          disable fancy output\n", NO_FENCY_SHORT_OPT, NO_FENCY_LONG_OPT);
   fprintf (stdout, "      %s, %s         disable colored output\n", NO_COLORS_SHORT_OPT, NO_COLORS_LONG_OPT);
   fprintf (stdout, "\n");
   fprintf (stdout, "      %s              let you configure %s\n", CONFIGURE_LONG_OPT, PACKAGE);
@@ -105,6 +107,7 @@ int knocker_args_init (knocker_args_t * args, int logfile, int quiet, int colors
   args->hname = NULL;
   args->hip = NULL;
   args->lfname = NULL;
+  args->hfamily = AF_UNSPEC;
   args->port = 0;
   args->sport = 0;
   args->eport = 0;
@@ -189,6 +192,16 @@ int knocker_args_parse (knocker_args_t * args, int argc, char *argv[])
             }
           return (0);           /* we should have all arguments here */
         }
+      else if ((!strcmp (argv[i], HOST_IPV4_SHORT_OPT)) || (!strcmp (argv[i], HOST_IPV4_LONG_OPT)))
+        {
+          /* Accept only IPv4 addressing. */
+          args->hfamily = AF_INET;
+        }
+      else if ((!strcmp (argv[i], HOST_IPV6_SHORT_OPT)) || (!strcmp (argv[i], HOST_IPV6_LONG_OPT)))
+        {
+          /* Accept only IPv6 addressing. */
+          args->hfamily = AF_INET6;
+        }
       else if ((!strcmp (argv[i], NO_FENCY_SHORT_OPT)) || (!strcmp (argv[i], NO_FENCY_LONG_OPT)))
         {
           /* Disable fency output */
diff --git a/src/knocker_args.h b/src/knocker_args.h
index 0ff8b35..ce8d5dc 100644
--- a/src/knocker_args.h
+++ b/src/knocker_args.h
@@ -33,6 +33,12 @@
   /* host to scan, got with lasthost  */
 #define LAST_HOST_LONG_OPT  "--last-host"
 
+  /* preferred address family for hosts */
+#define HOST_IPV4_SHORT_OPT "-4"
+#define HOST_IPV4_LONG_OPT "--ipv4"
+#define HOST_IPV6_SHORT_OPT "-6"
+#define HOST_IPV6_LONG_OPT "--ipv6"
+
   /* single port number */
 #define SINGLE_PORT_SHORT_OPT "-P"
 #define SINGLE_PORT_LONG_OPT  "--port"
@@ -82,6 +88,7 @@ typedef struct {
   char           *hname;      /* hostname string */
   char           *hip;        /* host IP string  */
   char           *lfname;     /* logfile name */
+  int            hfamily;     /* desired address domain */
   unsigned int   port;        /* Single port number, -P */
   unsigned int   sport;       /* Start port number, -SP */
   unsigned int   eport;       /* End port number, -EP   */
diff --git a/src/knocker_core.c b/src/knocker_core.c
index 413d4d6..4c39927 100644
--- a/src/knocker_core.c
+++ b/src/knocker_core.c
@@ -33,13 +33,13 @@ static void knocker_core_free_port_data (knocker_core_port_t * port);
 
 static int knocker_core_init_socket_data (knocker_core_socket_t * sock);
 static void knocker_core_free_socket_data (knocker_core_socket_t * sock);
-static int knocker_core_open_socket (knocker_core_socket_t * sock, int protocol);
+static int knocker_core_open_socket (knocker_core_socket_t * sock, int family, int protocol);
 static void knocker_core_close_socket (knocker_core_socket_t * sock);
 
 static int knocker_core_init_host_data (knocker_core_host_t * host);
 static void knocker_core_free_host_data (knocker_core_host_t * host);
 
-static int knocker_core_gethostbyname (knocker_core_host_t * hinfo, const char *hostname);
+static int knocker_core_gethostbyname (knocker_core_host_t * hinfo, const char *hostname, int family);
 static int knocker_core_getservbyport (char *service, unsigned int port, int protocol);
 
 static char *knocker_core_get_host_name_string (knocker_core_host_t * hinfo);
@@ -364,7 +364,7 @@ int knocker_core_portscan_tcp_connnect (knocker_core_portscan_data_t * data, uns
   fprintf (stderr, "debug: connecting to port: %d\n", port);
 #endif
 
-  if (knocker_core_open_socket (&data->socket, PROTO_TCP) == KNOCKER_SOCKET_ERROR)
+  if (knocker_core_open_socket (&data->socket, data->host.hostaddr.ss_family, PROTO_TCP) == KNOCKER_SOCKET_ERROR)
     {
 #ifdef DEBUG
       fprintf (stderr, "debug: socket error, couldn't connect.\n");
@@ -372,12 +372,16 @@ int knocker_core_portscan_tcp_connnect (knocker_core_portscan_data_t * data, uns
       return -1;
     }
 
-  data->host.sockaddr_in.sin_family = AF_INET;
-  data->host.sockaddr_in.sin_port = htons (port);
-  data->host.sockaddr_in.sin_addr = *((struct in_addr *) data->host.info->h_addr);
-  memset (&(data->host.sockaddr_in.sin_zero), 0, 8);
+  memcpy (&(data->host.sockaddr_st), &(data->host.hostaddr), data->host.addrlen);
+  switch (data->host.hostaddr.ss_family) {
+    case AF_INET6:
+      ((struct sockaddr_in6 *) &(data->host.sockaddr_st))->sin6_port = htons (port);
+    case AF_INET:
+    default:
+      ((struct sockaddr_in *) &(data->host.sockaddr_st))->sin_port = htons (port);
+  }
 
-  if (!connect (data->socket.fd, (struct sockaddr *) &data->host.sockaddr_in, sizeof (struct sockaddr)))
+  if (!connect (data->socket.fd, (struct sockaddr *) &data->host.sockaddr_st, data->host.addrlen))
     /* here the port is open */
     {
       knocker_core_close_socket (&data->socket);
@@ -401,9 +405,9 @@ int knocker_core_portscan_tcp_connnect (knocker_core_portscan_data_t * data, uns
    ============================================================================
    ============================================================================
 */
-char *knocker_core_resolve_host (knocker_core_portscan_data_t * data, const char *hostname)
+char *knocker_core_resolve_host (knocker_core_portscan_data_t * data, const char *hostname, int family)
 {
-  if (knocker_core_gethostbyname (&data->host, hostname) == -1)
+  if (knocker_core_gethostbyname (&data->host, hostname, family) == -1)
     return NULL;
   else
     return (data->host.ip);
@@ -506,14 +510,14 @@ static void knocker_core_free_socket_data (knocker_core_socket_t * sock)
    ============================================================================
    ============================================================================
 */
-static int knocker_core_open_socket (knocker_core_socket_t * sock, int protocol)
+static int knocker_core_open_socket (knocker_core_socket_t * sock, int family, int protocol)
 {
 #ifdef DEBUG
   fprintf (stderr, "debug: function knocker_core_open_socket (...) called.\n");
 #endif
   if (protocol == PROTO_TCP)
     {
-      if ((sock->fd = socket (AF_INET, SOCK_STREAM, 0)) == KNOCKER_SOCKET_ERROR)
+      if ((sock->fd = socket (family, SOCK_STREAM, 0)) == KNOCKER_SOCKET_ERROR)
         {
 #ifdef DEBUG
           fprintf (stderr, "debug: couldn't open the socket: ");
@@ -524,7 +528,7 @@ static int knocker_core_open_socket (knocker_core_socket_t * sock, int protocol)
     }
   else if (protocol == PROTO_UDP)
     {
-      if ((sock->fd = socket (AF_INET, SOCK_DGRAM, 0)) == KNOCKER_SOCKET_ERROR)
+      if ((sock->fd = socket (family, SOCK_DGRAM, 0)) == KNOCKER_SOCKET_ERROR)
         {
 #ifdef DEBUG
           fprintf (stderr, "debug: couldn't open the socket: ");
@@ -579,13 +583,47 @@ static void knocker_core_close_socket (knocker_core_socket_t * sock)
    ============================================================================
    ============================================================================
 */
-static int knocker_core_gethostbyname (knocker_core_host_t * hinfo, const char *hostname)
+static int knocker_core_gethostbyname (knocker_core_host_t * hinfo, const char *hostname, int family)
 {
-  if ((hinfo->info = gethostbyname (hostname)) == NULL)
+  struct addrinfo hints, *res, *ai;
+  char hostip[INET6_ADDRSTRLEN];
+  int err, fd;
+
+  memset (&hints, 0, sizeof (hints));
+  hints.ai_family = family;
+  hints.ai_flags = AI_ADDRCONFIG;
+
+  if ( (err = getaddrinfo (hostname, NULL, &hints, &res)) ) {
+#ifdef DEBUG
+    fprintf (stderr, "debug: knocker_core_gethostbyname(): %s", gai_strerror(err));
+#endif
+    return -1;
+  }
+
+  for (ai = res; ai; ai = ai->ai_next) {
+    if ( (ai->ai_family != AF_INET) && (ai->ai_family != AF_INET6) )
+      continue;
+
+    fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
+    if (fd < 0)
+      continue;
+    /* Valid socket indicates a functional address; accept it. */
+    close (fd);
+    hinfo->addrlen = ai->ai_addrlen;
+    memcpy(&(hinfo->hostaddr), ai->ai_addr, ai->ai_addrlen);
+    break;
+  }
+
+  if (res)
+    freeaddrinfo(res);
+
+  if (ai == NULL)
     return -1;
 
   knocker_core_set_host_name_string (hinfo, hostname);
-  knocker_core_set_host_ip_string (hinfo, inet_ntoa (*(struct in_addr *) *hinfo->info->h_addr_list));
+  getnameinfo ((struct sockaddr *) &(hinfo->hostaddr), hinfo->addrlen,
+      hostip, sizeof (hostip), NULL, 0, NI_NUMERICHOST);
+  knocker_core_set_host_ip_string (hinfo, hostip);
 
   return 0;
 }
diff --git a/src/knocker_core.h b/src/knocker_core.h
index 658001d..1b93d34 100644
--- a/src/knocker_core.h
+++ b/src/knocker_core.h
@@ -135,8 +135,9 @@ typedef struct {
 */
 
 typedef struct {
-  struct hostent                   *info;        /* hostent structure */
-  struct sockaddr_in               sockaddr_in;  /* sockaddr_in structure */
+  struct sockaddr_storage          hostaddr;     /* template address */
+  socklen_t                        addrlen;      /* active length */
+  struct sockaddr_storage          sockaddr_st;  /* working address */
   char                             *name;        /* hostname string   */
   char                             *ip;          /* host IP address string */
 } knocker_core_host_t;
@@ -185,7 +186,7 @@ void  knocker_core_free_portscan_data (knocker_core_portscan_data_t *data);
 int   knocker_core_validate_port_number (unsigned int port);
 
 /* returns host ip address on success, NULL on failure */
-char *knocker_core_resolve_host (knocker_core_portscan_data_t *data, const char *hostname);
+char *knocker_core_resolve_host (knocker_core_portscan_data_t *data, const char *hostname, int family);
 
 /* return the hostname string from the structure */
 char *knocker_core_get_hostname (knocker_core_portscan_data_t *data);
@@ -203,13 +204,13 @@ static void knocker_core_free_port_data   (knocker_core_port_t *port);
 
 static int  knocker_core_init_socket_data   (knocker_core_socket_t *sock);
 static void knocker_core_free_socket_data   (knocker_core_socket_t *sock);
-static int  knocker_core_open_socket   (knocker_core_socket_t *sock, int protocol);
+static int  knocker_core_open_socket   (knocker_core_socket_t *sock, int family, int protocol);
 static void knocker_core_close_socket  (knocker_core_socket_t *sock);
 
 static int  knocker_core_init_host_data   (knocker_core_host_t *host);
 static void knocker_core_free_host_data   (knocker_core_host_t *host);
 
-static int   knocker_core_gethostbyname    (knocker_core_host_t *hinfo, const char *hostname);
+static int   knocker_core_gethostbyname    (knocker_core_host_t *hinfo, const char *hostname, int family);
 static int   knocker_core_getservbyport    (char *service, unsigned int port, int protocol);
 
 static char *knocker_core_get_host_name_string (knocker_core_host_t *hinfo);
diff --git a/src/knocker_main.c b/src/knocker_main.c
index 855cb24..8673c10 100644
--- a/src/knocker_main.c
+++ b/src/knocker_main.c
@@ -132,7 +132,7 @@ static void quit (void)
 */
 static void resolve (void)
 {
-  if (knocker_core_resolve_host (&pscan_data, knocker_args.hname) == NULL)
+  if (knocker_core_resolve_host (&pscan_data, knocker_args.hname, knocker_args.hfamily) == NULL)
     {
       knocker_output_resolve_error (knocker_args.hname);
       knocker_log_resolve_error (knocker_args.hname);
diff --git a/src/knocker_user.c b/src/knocker_user.c
index 0d334ac..8b83d44 100644
--- a/src/knocker_user.c
+++ b/src/knocker_user.c
@@ -82,7 +82,7 @@ int knocker_user_init (knocker_user_t * user)
       _dir_create (user->dir);
     }
 
-  if (knocker_user_is_root)
+  if (knocker_user_is_root())
     user->super = 1;
   else
     user->super = 0;
